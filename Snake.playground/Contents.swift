import UIKit
import CoreGraphics
import Combine
import PlaygroundSupport

//
// models
//

enum Direction {
    case up
    case down
    case left
    case right
    case none

    var opposite: Direction {
        switch self {
        case .up:    return .down
        case .down:  return .up
        case .left:  return .right
        case .right: return .left
        case .none:  return .none
        }
    }

    static var random: Direction {
        [Direction](arrayLiteral: .up, .down, .left, .right).randomElement() ?? .none
    }
}

struct Cell {
    let x: Int
    let y: Int
}

struct Grid {
    let w: Int
    let h: Int

    var middle: Cell {
        Cell(x: w / 2, y: h / 2)
    }

    var random: Cell {
        Cell(x: Int.random(in: 0..<w), y: Int.random(in: 0..<h))
    }
}

struct Game {
    let grid:      Grid
    let direction: Direction
    let snake:     Snake
    let fruit:     Fruit
}

enum State {
    case wait(Game)
    case play(Game)
}

struct Snake {
    let cells: [Cell]

    init(cell: Cell, size: Int) {
        cells = [Cell](repeating: cell, count: size)
    }

    init(cells: [Cell]) {
        self.cells = cells
    }

    var head: Cell {
        cells.first!
    }

    func head(direction: Direction, inside grid: Grid) -> Cell {
        (head + direction).wrap(inside: grid)
    }

    func next(head: Cell) -> Snake {
        Snake(cells: [head] + cells.dropLast())
    }

    func eats(_ fruit: Fruit) -> Bool {
        head == fruit.cell
    }

    func grow(size: Int) -> Snake {
        Snake(cells: cells + [Cell](repeating: cells.last!, count: size))
    }

    func collides(with cell: Cell) -> Bool {
        cells.contains(cell)
    }
}

struct Fruit {
    let cell: Cell
    let size: Int
    let direction: Direction

    func next(inside grid: Grid) -> Fruit {
        Fruit(
            cell: (cell + direction).wrap(inside: grid),
            size: size,
            direction: direction
        )
    }

    func spawn(inside grid: Grid) -> Fruit {
        Fruit(cell: grid.random, size: size, direction: .random)
    }
}

//
// functions
//

extension Cell: Equatable {
    static func ==(lhs: Cell, rhs: Cell) -> Bool {
        lhs.x == rhs.x && lhs.y == rhs.y
    }
}

func +(cell: Cell, direction: Direction) -> Cell {
    switch direction {
    case .up:    return Cell(x: cell.x, y: cell.y - 1)
    case .down:  return Cell(x: cell.x, y: cell.y + 1)
    case .left:  return Cell(x: cell.x - 1, y: cell.y)
    case .right: return Cell(x: cell.x + 1, y: cell.y)
    case .none:  return cell
    }
}

extension Cell {
    func wrap(inside grid: Grid) -> Cell {
        Cell(x: (x < 0 ? grid.w + x : x) % grid.w, y: (y < 0 ? grid.h + y : y) % grid.h)
    }
}

extension CGRect {
    func offset(cell: Cell) -> CGRect {
        CGRect(
            origin: CGPoint(
                x: origin.x + width  * CGFloat(cell.x),
                y: origin.y + height * CGFloat(cell.y)
            ),
            size: size
        )
    }
}

extension Game {
    func with(_ direction: Direction, _ snake: Snake, _ fruit: Fruit) -> Game {
        Game(grid: grid, direction: direction, snake: snake, fruit: fruit)
    }
}

//
// views
//

class View: UIView {
    typealias Tile  = (Cell, Style)
    typealias Model = (Grid, [Tile])

    enum Style {
        case back1
        case back2
        case snake
        case fruit
        case blank
        case bonce
    }

    let colors: [Style: UIColor] = [
        .back1: UIColor(displayP3Red: 0.0, green: 0.3, blue: 0.6, alpha: 1),
        .back2: UIColor(displayP3Red: 0.0, green: 0.2, blue: 0.5, alpha: 1),
        .snake: UIColor(displayP3Red: 0.0, green: 0.8, blue: 0.0, alpha: 1),
        .fruit: UIColor(displayP3Red: 0.8, green: 0.0, blue: 0.0, alpha: 1),
        .blank: UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 1),
        .bonce: UIColor(displayP3Red: 0.8, green: 0.8, blue: 0.0, alpha: 1),
    ]

    init(size: Int) {
        super.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
    }

    required init?(coder: NSCoder) {
        fatalError()
    }

    func render(model: Model) {
        let (grid, tiles) = model

        CATransaction.begin()

        if grid.w * grid.h != subviews.count {
            subviews.forEach { it in it.removeFromSuperview() }
            let cursor = newCursor(grid)
            for y in 0..<grid.h {
                for x in 0..<grid.w {
                    addSubview(UIView(frame: cursor.offset(cell: Cell(x: x, y: y))))
                }
            }
        }

        tiles.forEach { cell, style in
            let index = cell.x + cell.y * grid.w
            let color = colors[style]
            subviews[index].backgroundColor = color
        }

        CATransaction.commit()
    }

    func newCursor(_ grid: Grid) -> CGRect {
        let w = bounds.width  / CGFloat(grid.w)
        let h = bounds.height / CGFloat(grid.h)
        return CGRect(
            origin: .zero,
            size:   CGSize(width: w, height: h)
        )
    }
}

//
// helpers
//

protocol Tapped {}

extension Tapped {
    func tap(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension UISwipeGestureRecognizer: Tapped {}
extension DisplayLinkSubscription: Tapped {}

//
// publishers
//

extension UISwipeGestureRecognizer {
    func publisher(direction: UISwipeGestureRecognizer.Direction) -> AnyPublisher<UISwipeGestureRecognizer.Direction, Never> {
        SwipePublisher(recognizer: self, direction: direction)
            .eraseToAnyPublisher()
    }
}

struct SwipePublisher: Publisher {
    typealias Output  = UISwipeGestureRecognizer.Direction
    typealias Failure = Never

    let recognizer: UISwipeGestureRecognizer
    let direction:  UISwipeGestureRecognizer.Direction

    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        subscriber.receive(
            subscription: SwipeSubscription(
                subscriber: subscriber,
                recognizer: recognizer,
                direction:  direction
            )
        )
    }
}

final class SwipeSubscription<S: Subscriber>: Subscription where S.Input == UISwipeGestureRecognizer.Direction {
    var subscriber: S?
    let recognizer: UISwipeGestureRecognizer

    init(
        subscriber: S,
        recognizer: UISwipeGestureRecognizer,
        direction:  UISwipeGestureRecognizer.Direction
    ) {
        self.subscriber = subscriber
        self.recognizer = recognizer

        recognizer.direction = direction
        recognizer.addTarget(self, action: #selector(swiped))
    }

    @objc private func swiped(swipe: UISwipeGestureRecognizer) {
        subscriber?.receive(recognizer.direction)
    }

    func request(_ demand: Subscribers.Demand) {
        // no-op
    }

    func cancel() {
        subscriber = nil
    }
}

extension CADisplayLink {
    static func publisher() -> AnyPublisher<CFTimeInterval, Never> {
        DisplayLinkPublisher().eraseToAnyPublisher()
    }
}

struct DisplayLinkPublisher: Publisher {
    typealias Output  = CFTimeInterval
    typealias Failure = Never

    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        subscriber.receive(
            subscription: DisplayLinkSubscription(subscriber).tap { it in it.schedule() }
        )
    }
}

final class DisplayLinkSubscription<S: Subscriber>: Subscription where S.Input == CFTimeInterval {
    var subscriber: S?
    var link:       CADisplayLink!

    init(_ subscriber: S) {
        self.subscriber = subscriber
    }

    deinit {
        link.invalidate()
    }

    func schedule() {
        link = CADisplayLink(target: self, selector: #selector(tick))
        link.add(to: .main, forMode: .default)
    }

    @objc private func tick(link: CADisplayLink) {
        subscriber?.receive(link.timestamp)
    }

    func request(_ demand: Subscribers.Demand) {
        // no-op
    }

    func cancel() {
        subscriber = nil
    }
}

//
// controller
//

func toDirection(it: UISwipeGestureRecognizer.Direction) -> Direction {
    switch it {
    case .up:    return .up
    case .down:  return .down
    case .left:  return .left
    case .right: return .right
    default:     return .none
    }
}

func nextState(state: State, tick: (Direction, CFTimeInterval)) -> State {
    let (direction, _) = tick
    switch state {
    case let .wait(game): return nextWaitState(game, direction)
    case let .play(game): return nextPlayState(game, direction)
    }
}

func nextWaitState(_ game: Game, _ direction: Direction) -> State {
    direction == .none ? .wait(game) : .play(game)
}

func nextPlayState(_ game: Game, _ direction: Direction) -> State {
    let next = (direction != game.direction.opposite) ? direction : game.direction
    let head = game.snake.head(direction: next, inside: game.grid)

    if game.snake.collides(with: head) {
        return .wait(
            game.with(
                next,
                Snake(cell: game.grid.middle, size: 2),
                Fruit(cell: game.grid.random, size: 2, direction: .random)
            )
        )
    }

    let snake = game.snake.next(head: head)
    let fruit = game.fruit.next(inside: game.grid)

    if snake.eats(fruit) {
        return .play(
            game.with(
                next,
                snake.grow(size: fruit.size),
                fruit.spawn(inside: game.grid)
            )
        )
    }

    return .play(game.with(next, snake, fruit))
}

//
// state -> view model
//

func toViewModel(state: State) -> View.Model {
    switch state {
    case .wait:           return toWaitViewModel()
    case let .play(game): return toPlayViewModel(game)
    }
}

func toWaitViewModel() -> View.Model {
    View.Model(Grid(w: 1, h: 1), [(Cell(x: 0, y: 0), .blank)])
}

func toPlayViewModel(_ game: Game) -> View.Model {
    var tiles  = [View.Tile]()
    var toggle = true

    for y in 0..<game.grid.h {
        for x in 0..<game.grid.w {
            tiles.append((Cell(x: x, y: y), toggle ? .back1 : .back2))
            toggle = !toggle
        }
    }

    tiles += game.snake.cells.map { it in (it, .snake) }
    tiles.append((game.fruit.cell, .fruit))
    tiles.append((game.snake.head, .bonce))

    return View.Model(game.grid, tiles)
}

let view = View(size: 480)
let grid = Grid(w: 9, h: 9)
let game = Game(
    grid: grid,
    direction: .none,
    snake: Snake(cell: grid.middle, size: 3),
    fruit: Fruit(cell: grid.random, size: 3, direction: .random)
)

let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .left, .right]
let inputs = directions.map { direction in
    UISwipeGestureRecognizer()
        .tap(view.addGestureRecognizer)
        .publisher(direction: direction)
}

let pipeline = Publishers.Merge4(inputs[0], inputs[1], inputs[2], inputs[3])
    .map(toDirection)
    .combineLatest(CADisplayLink.publisher())
    .merge(with: Just((.none, 0)))
    .throttle(for: 0.5, scheduler: RunLoop.main, latest: true)
    .scan(.wait(game), nextState)
    .map(toViewModel)
    .sink(receiveValue: view.render)

PlaygroundPage.current.liveView = view
